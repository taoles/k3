import java.util.Stack;
import java.util.EmptyStackException;
import java.util.StringTokenizer;
import static org.junit.Assert.*;

public class DoubleStack {

   private Stack<Double> stack;

   public static void main (String[] argum) {
      double tt = 0.;
      DoubleStack test = new DoubleStack();
      test.push(5.);
      test.push(3.);
      test.op("*");
      tt = test.pop();
      assertTrue("Error", tt==15.);
      assertTrue(test.stEmpty());
      test.push(15.);
      test.push(3.);
      test.op("/");
      tt = test.pop();
      assertTrue("Error", tt==5.);
      test.push(25.);
      test.push(5.);
      test.op("-");
      tt = test.pop();
      assertTrue("Error", tt==20.);
      test.push(5.);
      test.push(10.);
      test.op("+");
      tt = test.pop();
      assertTrue("Error", tt==15.);
   }

   DoubleStack() {
      this.stack = new Stack<Double>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack temporary = new DoubleStack();
      DoubleStack clone = new DoubleStack();

      int stackSize = this.stack.size();
      for (int i = 0; i < stackSize; i++) {
         double temp = this.stack.pop();
         temporary.push(temp);
      }
      for (int i = 0; i < stackSize; i++) {
         double temp = temporary.pop();
         clone.push(temp);
         this.stack.push(temp);
      }

      return clone;
   }

   public boolean stEmpty() {
      return this.stack.isEmpty();
   }

   public void push (double a) {
      this.stack.push(a);
   }

   public double pop() {
      try {
         return this.stack.pop();
      } catch (EmptyStackException e) {
         throw new RuntimeException("Stack is empty");
      }
   }

   public void op (String s) {
      try {
         double first = this.stack.pop();
         double second = this.stack.pop();

         if (s.equals("+")) {
            push(second + first);
         } else if (s.equals("-")) {
            push(second - first);
         } else if (s.equals("*")) {
            push(second * first);
         } else if (s.equals("/")) {
            push(second / first);
         } else {
            throw new RuntimeException("Method \"op\" called with incorrect argument: " + s + ", possible arguments: +,-,*,/.");
         }

      } catch (EmptyStackException e) {
         throw new RuntimeException("It is not possible to use " + s + " argument, with less than 2 numbers.");
      }
   }
  
   public double tos() {
      try {
         return stack.peek();
      } catch (EmptyStackException e) {
         throw new RuntimeException("Stack is empty");
      }
   }

   @Override
   public boolean equals (Object o) {
      if (o == null)
         return false;
      if (!(o instanceof DoubleStack))
         return false;

      DoubleStack other = (DoubleStack) o;
      if (this.stack.equals(other.stack))
         return true;

      return false;
   }

   @Override
   public String toString() {
      if (stEmpty()) {
         return "Stack is empty";
      }

      StringBuffer b = new StringBuffer();

      for (int i = 0; i < stack.size(); i++) {
         b.append(stack.get(i).toString());
         b.append(" ");
      }
      return b.toString();
   }

   public void swap() {
      if (stack.size() < 2) {
         throw new RuntimeException("Not enough elements in stack");


      }
      double firstElement = this.stack.remove(stack.size()-1);
      double secondElement = this.stack.remove(stack.size()-1);
      this.stack.add(0, firstElement);
      this.stack.add(1,secondElement);
   }


   public void dup() {
      if(stack.size() < 1) {
         throw new RuntimeException("Not enough elements in stack");
      }
      double lastElement = this.stack.get(stack.size()-1);
      this.stack.add(stack.size()-1, lastElement);
   }

   public void rot() {
      if(stack.size() < 3){
         throw new RuntimeException("Not enough elements in stack");
      }
      double thirdElement = this.stack.remove(stack.size()-1);
      double firstElement = this.stack.remove(stack.size()-2);
      this.stack.add(1, thirdElement);
      this.stack.add(stack.size(), firstElement);
   }

   public static double interpret (String pol) {
      StringTokenizer splitted = new StringTokenizer(pol);
      String next = "";
      String operators = "+-/*";
      DoubleStack stack = new DoubleStack();
      String currentOperator = "";
      double answer = 0;
      String swap = "SWAP";
      String dup = "DUP";
      String rot = "ROT";
      String swapduprot = "SWAPDUPROT";

      while (splitted.hasMoreTokens()) {
         next = splitted.nextToken();


         if (operators.contains(next)) {
            currentOperator = next;
            stack.op(currentOperator);
         } else if (swapduprot.contains(next)) {
            if (swap.contains(next)) {
               pol = pol.replace("SWAP ", "");
               stack.swap();
            }

            if (dup.contains(next)) {
               pol = pol.replace("DUP ", "");
               stack.dup();
            }

            if (rot.contains(next)) {
               pol = pol.replace("ROT ", "");
               stack.rot();
            }
         }
         else {
            try {
               double num = Double.valueOf(next);
               stack.push(num);
            } catch (NumberFormatException e) {
               throw new RuntimeException("Method \"interpret\" argument " + pol + " is not in the correct format.");
            }
         }
         }

      if (stack.stEmpty()) {
         throw new RuntimeException("Method \"interpret\" argument " + pol + " have not convertible to double type numbers.");
      }

      answer = stack.pop();
      if (!stack.stEmpty()) {
         throw new RuntimeException("Method \"interpret\" argument " + pol + " has more than 2 convertible to double type numbers for one argument.");
      }
      return answer;
   }

}

// https://github.com/mainrid/AlgoritmsHW3/blob/master/DoubleStack.java
// https://enos.itcollege.ee/~jpoial/algoritmid/adt.html
// https://docs.oracle.com/javase/7/docs/api/java/util/Stack.html
// https://www.youtube.com/watch?v=fptlqsesjxY

